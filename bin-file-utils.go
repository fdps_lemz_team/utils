package utils

import (
	"fmt"
	"io/ioutil"
	"os"
)

var (
	versionDir     string
	runningDir     string
	extension      string
	fileName       string
	fileNameForLog string
)

// InitFileBinUtils инициализация параметров запускаемых бинарников
// versDir - директория с версиями (исполняемые файлы лежат в поддиректориях versDir)
// runDir - директория, в которую копируются и затем запускаются исполняемые файлы
// binFileExtension - разрешение исолняемого файла (".exe")
// binFileName - имя исполняемого файла
func InitFileBinUtils(versDir string, runDir string, binFileExtension string, binFileName string, nameForLog string) {
	versionDir = versDir
	runningDir = runDir
	extension = binFileExtension
	fileName = binFileName
	fileNameForLog = nameForLog
}

// GetChannelVersions предоставляет список версий приложения
func GetChannelVersions() ([]string, error) {
	var retValue []string

	dirs, dirErr := ioutil.ReadDir(versionDir)
	if dirErr != nil {
		return []string{}, fmt.Errorf("Ошибка чтения директории с версиями исполняемых файлов \"%s\". Директория <%s>. Ошибка: <%s>",
			fileNameForLog, versionDir, dirErr.Error())
	} else {
		for _, dir := range dirs {
			if dir.IsDir() {
				curVersionDir := versionDir + "/" + dir.Name()
				files, fileErr := ioutil.ReadDir(curVersionDir)
				if fileErr != nil {
					return retValue, fmt.Errorf("Ошибка чтения директории с исполняемым файлом \"%s\". Директория <%s>. Ошибка: <%s>",
						fileNameForLog, curVersionDir, fileErr.Error())
				} else {
					// проверка, что содержится исполняемого файл канала
					for _, file := range files {
						fmt.Println("file ", file.Name())
						if !file.IsDir() && file.Name() == fileName+extension {
							retValue = append(retValue, dir.Name())
							break
						}
					}
				}
			}
		}
	}

	if len(retValue) == 0 {
		return retValue, fmt.Errorf("Не найдена ни одна версия исполняемого файла \"%s\". Директория: <%s>",
			fileNameForLog, (AppPath() + "/versions"))
	}

	return retValue, nil
}

// CopyBinary копирование исполняемого файла приложения
func CopyBinary(vers string, id int, postfix ...string) (binFilePath string, err error) {
	if chDirErr := os.Chdir(runningDir); chDirErr != nil {
		if mkdirErr := os.MkdirAll(runningDir, os.ModePerm); mkdirErr != nil {
			return "", fmt.Errorf("Невозможно создать рабочую директорию для запуска приложений \"%s\". Ошибка: <%s>",
				fileNameForLog, mkdirErr.Error())
		}
	}

	srcDir := versionDir + "/" + vers
	if chDirErr := os.Chdir(runningDir); chDirErr != nil {
		return "", fmt.Errorf("Ошибка копирования исполняемого файла \"%s\" версии <%s>. Версия не найдена. Ошибка: <%s>",
			fileNameForLog, vers, chDirErr.Error())
	}
	srcFile := srcDir + "/" + fileName + extension

	input, err := ioutil.ReadFile(srcFile)
	if err != nil {
		return "", fmt.Errorf("Ошибка копирования файла \"%s\" в рабочую директорию приложения <%s>. Ошибка: <%s>",
			fileNameForLog, srcFile, err.Error())
	}

	dstFile := runningDir + "/" + fmt.Sprintf("%s-%d", fileName, id)
	for _, val := range postfix {
		dstFile += fmt.Sprintf("-%s", val)
	}
	dstFile += extension

	if err := ioutil.WriteFile(dstFile, input, os.ModePerm); err != nil {
		return "", fmt.Errorf("Ошибка копирования исполняемого файла \"%s\" в рабочую директорию приложения <%s>. Ошибка: <%s>",
			fileNameForLog, dstFile, err.Error())
	}
	return dstFile, nil
}

// RemoveBinary удаление исполняемого файла
func RemoveBinary(binFilePath string) error {
	if err := os.Remove(binFilePath); err != nil {
		return fmt.Errorf("Ошибка при удалении исполняемого файла \"%s\" из рабочей директории. Исполняемый файл: <%s>. Ошибка: <%s>",
			fileNameForLog, binFilePath, err.Error())
	}
	return nil
}
