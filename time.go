package utils

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"time"
)

func HhMmFromSecond(s int64) (string, error) {

	if s >= 0 {
		return fmt.Sprintf("%02d%02d", int64((s/3600)%24), int64((s%3600)/60)), nil
	} else {
		return "0000", errors.New(fmt.Sprintf("value of second is negative %d", s))
	}
}

func SecondFromHhMm(hhmm string) int {
	if len(hhmm) != 4 {
		log.Println("Error SecondFromHhMm  time =", hhmm)
		return 0
	}

	h := hhmm[0:2]
	m := hhmm[2:4]

	hs, ms := 0, 0

	var e error
	
	if hs, e = strconv.Atoi(h); e != nil {
		log.Println("Error SecondFromHhMm", e)
	}

	if ms, e = strconv.Atoi(m); e != nil {
		log.Println("Error SecondFromHhMm", e)
	}

	return hs*60*60 + ms*60
}

func AddHHMM(t time.Time, hhmm string) time.Time {
	if len(hhmm) != 4 {
		log.Println("Error AddHHMM  time =", hhmm)
	}
	return t.Add(time.Duration(SecondFromHhMm(hhmm)) * time.Second)
}
