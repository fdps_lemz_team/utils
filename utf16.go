package utils

import (
	"bytes"
	"encoding/binary"
	"errors"
	"log"
	"unicode/utf16"
)

//func DecodeUTF16(b []byte, order binary.ByteOrder) (string, error) {
//	u16s := []uint16{}
//	for i, j := 0, len(b); i < j; i += 2 {
//		u16s = append(u16s, order.Uint16(b[i:]))
//	}
//	runes := utf16.Decode(u16s)
//	return string(runes), nil
//}

//binary.LittleEndian
func DecodeUTF16(b []byte, order binary.ByteOrder) (string, error) {
	if len(b)%2 != 0 {
		return string(""), errors.New("DecodeUTF16. Length b is wrong")
	}
	var u16s = make([]uint16, len(b)/2)
	buf := bytes.NewReader(b)
	err := binary.Read(buf, order, &u16s)
	if err != nil {
		log.Println("DecodeUTF16 binary.Read failed:", err)
	}
	runes := utf16.Decode(u16s)
	return string(runes), nil
}

func EncodeUTF16(s string, order binary.ByteOrder) ([]byte, error) {
	buf := new(bytes.Buffer)
	if len(s) == 0 {
		return []byte{}, nil
	}
	shorts := utf16.Encode([]rune(s))
	err := binary.Write(buf, order, shorts)
	if err != nil {
		log.Println("EncodeUTF16 binary.Write failed:", err)
		return []byte{}, err
	}
	return buf.Bytes(), nil
}
