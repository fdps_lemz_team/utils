package utils

import (
	"fmt"
	"gopkg.in/natefinch/lumberjack.v2"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const logExtension = ".log"

func SetLog(name string) {
	appDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	logDir := strings.Join([]string{appDir, "logs", name},
		string(filepath.Separator))

	if e := os.MkdirAll(logDir, os.ModePerm); e != nil {
		log.Println(e)
	}

	logName := fmt.Sprintf("%s%s%s", logDir, string(filepath.Separator), name+logExtension)

	log.Println("log name", logName)

	log.SetOutput(&lumberjack.Logger{
		Filename:   logName,
		MaxSize:    1,     // megabytes
		MaxBackups: 7,     //files
		MaxAge:     7,     // days
		Compress:   false, // disabled by default
	})
}
