package utils

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func AppendToFile(fname string, v interface{}) error {
	f, err := os.OpenFile(fname, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
		return err
	}
	defer f.Close()

	data, _ := json.MarshalIndent(v, "", "   ")

	datastr := string(data)
	log.Println(datastr)
	datastr = strings.ReplaceAll(datastr, "\\n", "\n")
	datastr = strings.ReplaceAll(datastr, "\\", "")

	if _, err = f.WriteString(string("\n" + datastr)); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func SaveToFile(fname string, v interface{}) error {
	data, _ := json.MarshalIndent(v, "", "   ")
	if err := ioutil.WriteFile(fname, data, os.ModePerm); err != nil {
		log.Println("SaveToFile", err)
		return err
	}
	return nil
}

func ReadFromFile(fname string, v interface{}) error {
	data, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Println("ReadFromFile", err)
		return err
	} else {
		err := json.Unmarshal(data, &v)
		if err != nil {
			log.Println("ReadFromFile", err)
			return err
		}
	}
	return nil
}

func CreateDirIfNotExist(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755) //os.ModeDir 0755
		if err != nil {
			panic(err)
		}
	}
}
