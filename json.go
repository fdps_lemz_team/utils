// util
package utils

import (
	"bytes"
	"encoding/json"
	"log"
)

func JsonPrettyPrint(in []byte) []byte {
	var out bytes.Buffer
	err := json.Indent(&out, in, "", "\t")
	if err != nil {
		return in
	}
	return out.Bytes()
}

func ToJson(v interface{}) (string, error) {
	out, err := json.MarshalIndent(v, "  ", "   ")
	if err != nil {
		log.Println(err)
		return "", err
	}
	return string(out), nil
}

func ToJsonString(v interface{}) string {
	out, err := json.MarshalIndent(v, "  ", "   ")

	if err != nil {
		log.Fatal(err)
	}

	return string(out)
}

func ToJsonByte(v interface{}) []byte {
	out, err := json.MarshalIndent(v, "  ", "   ")

	if err != nil {
		log.Fatal(err)
	}

	return out
}

func FromJson(s []byte, v interface{}) error {
	err := json.Unmarshal(s, &v)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

/*
func (p Pln) ToXml() (string, error) {
	out, err := xml.MarshalIndent(p, "  ", "    ")
	if err != nil {
		log.Println(err)
		return "", err
	}
	return string(out), nil
}

func (p *Pln) FromXml(val string) error {
	err := xml.Unmarshal([]byte(val), &p)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
*/
