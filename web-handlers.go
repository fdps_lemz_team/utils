package utils

import (
	"net/http"
)

type IHandler interface {
	Path() string
	Caption() string
	HttpHandler() func(http.ResponseWriter, *http.Request)
}

var HandlerList []IHandler

func AppendHandler(h IHandler) {
	HandlerList = append(HandlerList, h)
}
