package utils

import (
	"fmt"
	"github.com/marcsauter/single"
	"log"
	"os"
	"path/filepath"
)

func NewSingle(appName string) *single.Single {
	appDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	single.Lockfile = fmt.Sprintf("%s%s%s", appDir, string(filepath.Separator), appName+".lock")

	log.Println("Lockfile", single.Lockfile)

	s := single.New(appName)

	return s
}
