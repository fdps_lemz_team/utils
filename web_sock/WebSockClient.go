package web_sock

import (
	"fdps/utils"
	"fmt"
	"net/url"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

// настройки WS
type WebSockClientSettings struct {
	ServerAddress     string // сетевой адрес сервера WS
	ServerPort        int    // сетевой порт сервера WS
	UrlPath           string // path
	ReconnectInterval int    // интервал переподключения к серверу в случае ошибки
	// если = 0, значит не недо переподключаться (серверный сокет)
}

const (
	WriteWait        = 10 * time.Second    // Time allowed to write a message to the peer.
	MaxMessageSize   = 8192                // Maximum message size allowed from peer.
	PongWait         = 6 * time.Second     // Time allowed to read the next pong message from the peer.
	PingPeriod       = (PongWait * 9) / 10 // Send pings to peer with this period. Must be less than pongWait.
	CloseGracePeriod = 10 * time.Second    // Time to wait before force close on connection.

	ClientConnected    = 0
	ClientDisconnected = 1
)

// обертка Web Socket
type WebSockClient struct {
	sync.Mutex

	ErrorChan       chan error  // канал для передачи ошибок в работе сокета (если успешно подключились, передаем nil)
	ReceiveDataChan chan []byte // канал для приема данных от контроллера каналов
	SendDataChan    chan []byte // канал для отправки данных контроллеру каналов
	StateChan       chan int    // канал для передачи состояния

	cancelWorkChan chan struct{} // канал для сигнала отмены отправки/приема данных
	doneChan       chan struct{} // канал для прекращения работы
	isConnected    bool          // признак работоспособности (подключен к серверу)
	sendChan       chan []byte   // канал для отправки данных
	workErrorChan  chan error    // канал для передачи ошибки во время работы
	ws             *websocket.Conn
	wsSettings     WebSockClientSettings
}

// * при возникновении ошибки в чтении или записи, рутина, в которой возникла ошибка отправляет в канал
// errorChan ошибку и завершается, вторая рутина вычитывает из канала cancelWorkChan и тоже завершается
// поэтому чтение из cancelWorkChan в обеих рутинах, а пишется в него один раз в stopClient

// конструктор
func NewWebSockClient(done chan struct{}) *WebSockClient {
	return &WebSockClient{
		ErrorChan:       make(chan error, 100),
		ReceiveDataChan: make(chan []byte, 1024),
		SendDataChan:    make(chan []byte, 1024),
		sendChan:        make(chan []byte, 1024),
		workErrorChan:   make(chan error, 10),
		StateChan:       make(chan int, 10),
		isConnected:     false,
		doneChan:        done,
	}
}

func (wsc *WebSockClient) Work(wsSetts WebSockClientSettings) {
	if wsc.wsSettings != wsSetts {
		wsc.wsSettings = wsSetts

		go wsc.startClient()

		for {
			select {
			// ошибка чтения/записи
			case err := <-wsc.workErrorChan:
				wsc.checkState(false, err)
				wsc.stopClient()
				if wsc.wsSettings.ReconnectInterval != 0 {
					go func() { time.AfterFunc(time.Duration(wsc.wsSettings.ReconnectInterval)*time.Second, wsc.startClient) }()
				}

			// получены данные для отправки кленту
			case dataToSend := <-wsc.SendDataChan:
				if wsc.isConnected {
					wsc.sendChan <- dataToSend
				}

			// прекращаем работу
			case <-wsc.doneChan:
				wsc.stopClient()
				return
			}
		}
	}
}

// предоставить признак работоспособности (подключен к серверу)
func (wsc *WebSockClient) IsConnected() bool {
	return wsc.isConnected
}

// стартовать подключение к серверу
func (wsc *WebSockClient) startClient() {
	wsAddr := fmt.Sprintf("%s:%d", wsc.wsSettings.ServerAddress, wsc.wsSettings.ServerPort)
	u := url.URL{Scheme: "ws", Host: wsAddr, Path: wsc.wsSettings.UrlPath}
	var err error
	if wsc.ws, _, err = websocket.DefaultDialer.Dial(u.String(), nil); err != nil {
		if wsc.wsSettings.ReconnectInterval != 0 {
			go func() { time.AfterFunc(time.Duration(wsc.wsSettings.ReconnectInterval)*time.Second, wsc.startClient) }()
		}
		wsc.checkState(false, err)
		return
	} else {
		wsc.checkState(true, nil)
		wsc.cancelWorkChan = make(chan struct{})
		go wsc.receiveLoop()
		go wsc.sendLoop()
	}
}

// остановить соединение с сервером
func (wsc *WebSockClient) stopClient() {
	wsc.Lock()
	defer wsc.Unlock()

	_ = utils.ChanSafeClose(wsc.cancelWorkChan)
	wsc.checkState(false, nil)
	if wsc.ws != nil {
		if err := wsc.ws.Close(); err != nil {
			wsc.ErrorChan <- fmt.Errorf("Ошибка при закрытии клиентского WS подключения к Мониторсофт. Ошибка: <%s>.", err.Error())
		}
	}
}

// обработчик получения данных
func (wsc *WebSockClient) receiveLoop() {
	for {
		select {
		// получена отмена отправки/приема данных
		case <-wsc.cancelWorkChan:
			return

		default:
			_, data, err := wsc.ws.ReadMessage()
			if err != nil {
				wsc.workErrorChan <- err
				return
			} else {
				wsc.ReceiveDataChan <- data
			}
		}
	}
}

// обработчик отправки данных
func (wsc *WebSockClient) sendLoop() {
	for {
		select {
		// получена отмена отправки/приема данных
		case <-wsc.cancelWorkChan:
			return
		// получены данные для отправки кленту
		case curDataToSend := <-wsc.sendChan:
			if err := wsc.ws.WriteMessage(websocket.TextMessage, curDataToSend); err != nil {
				wsc.workErrorChan <- err
				return
			}
		}
	}
}

func (wsc *WebSockClient) checkState(isConnected bool, err error) {
	wsc.isConnected = isConnected
	if wsc.isConnected {
		wsc.StateChan <- ClientConnected
	} else {
		wsc.StateChan <- ClientDisconnected
	}

	if err != nil {
		wsc.ErrorChan <- err
	}
}
