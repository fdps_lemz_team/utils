package web_sock

import (
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

const (
	ServerTryToStart   = 0
	ServerError        = 1
	ServerReadySend    = 2
	ServerNotReadySend = 3
)

// настройки сервера WS
type WebSockServerSettings struct {
	Port            int      // сетевой порт
	PermitClientIps []string // список размерешшых клиентских адресов (если пустой, то всем можно подключаться)
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// данные и указатель на сокет
type WsPackage struct {
	Data []byte          // данные (принятые | для отправки)
	Sock *websocket.Conn // указатель на сокет (от которого приняли | которому необходимо передать)
}

// Контроллер, отвечающий за прием/отправку сообщений по WebSocket
type WebSockServer struct {
	sync.Mutex

	ClntConnChan    chan *websocket.Conn       // канал для передачи подлюченного клиента
	ClntDisconnChan chan *websocket.Conn       // канал для передачи отключенного клиента
	ClntRejectChan  chan *websocket.Conn       // канал для передачи отклоненного клиента
	ErrorChan       chan error                 // канал для передачи ошибок в работе сокета (если успешно подключились, передаем nil)
	ReceiveDataChan chan WsPackage             // канал для отправки считанных данных
	SendDataChan    chan WsPackage             // канал для приема дынных, которые необходимо отправить клиентам
	SettingsChan    chan WebSockServerSettings // канал приема новых настроек контроллера
	StateChan       chan int                   // канал для передачи состояния

	curSetts WebSockServerSettings
	doneChan chan struct{}            // канал для прекращения работы
	server   *http.Server
	clients  map[*websocket.Conn]bool // подключенные клиенты. ключ- сокет, значение - успешное подключение
}

// конструктор
func NewWebSockServer(done chan struct{}) *WebSockServer {
	return &WebSockServer{
		ClntConnChan:    make(chan *websocket.Conn, 1),
		ClntDisconnChan: make(chan *websocket.Conn, 1),
		ClntRejectChan:  make(chan *websocket.Conn, 1),
		ErrorChan:       make(chan error, 10),
		ReceiveDataChan: make(chan WsPackage, 1024),
		SendDataChan:    make(chan WsPackage, 1024),
		SettingsChan:    make(chan WebSockServerSettings),
		StateChan:       make(chan int, 10),
		clients:         make(map[*websocket.Conn]bool),
		doneChan:        done,
	}
}

// запуск работы контроллера
func (ws *WebSockServer) Work(urlPath string) {
	router := mux.NewRouter()
	router.HandleFunc(urlPath, ws.handleMain)

FORLBL:
	for {
		select {
		//
		case newSettings := <-ws.SettingsChan:
			if !reflect.DeepEqual(ws.curSetts, newSettings) {
				ws.curSetts = newSettings

				if ws.server != nil {
					ws.stopNetServer()
				}

				ws.server = &http.Server{
					Handler:      router,
					Addr:         ":" + strconv.Itoa(ws.curSetts.Port),
					ReadTimeout:  10 * time.Second,
					WriteTimeout: 10 * time.Second,
				}
				go func() {
					ws.StateChan <- ServerTryToStart
					if err := ws.server.ListenAndServe(); err != nil {
						ws.StateChan <- ServerError
						ws.ErrorChan <- fmt.Errorf("Ошибка запуска WS сервера. Ошибка: <%s>", err.Error())
					}
				}()
			}

		// получены данные для клиента (на отправку)
		case curPkgToWrite := <-ws.SendDataChan:
			ws.Lock()
			if curPkgToWrite.Sock != nil {
				//if _, ok := nc.clientSockets[curPkgToWrite.Sock]; ok {
				if _, ok := ws.clients[curPkgToWrite.Sock]; ok {
					//curPkgToWrite.Sock.SendChan <- curPkgToWrite.Data
					if err := curPkgToWrite.Sock.WriteMessage(websocket.TextMessage, curPkgToWrite.Data); err != nil {
						ws.ErrorChan <- fmt.Errorf("Ошибка отправки данных клиенту с адресом; <%s>. Ошибка: <%s> .", curPkgToWrite.Sock.RemoteAddr().String(), err.Error())
						//ws.ClntDisconnChan <- curPkgToWrite.Sock
						//curPkgToWrite.Sock.Close()
						//delete(ws.clients, curPkgToWrite.Sock)
						ws.HandleSockClose(curPkgToWrite.Sock)
						ws.CheckReadySend()
						ws.Unlock()
						continue FORLBL
					}
				} else {
					ws.ErrorChan <- fmt.Errorf("Не найден клиент для отправки. Адрес клиента: <%s>.", curPkgToWrite.Sock.RemoteAddr().String())
				}
			} else {

			SOCKLBL:
				for sock := range ws.clients {
					//sock.SendChan <- curPkgToWrite.Data
					if err := sock.WriteMessage(websocket.TextMessage, curPkgToWrite.Data); err != nil {
						ws.ErrorChan <- fmt.Errorf("Ошибка отправки данных клиенту с адресом; <%s>. Ошибка: <%s> .", sock.RemoteAddr().String(), err.Error())
						//ws.ClntDisconnChan <- sock
						//sock.Close()
						//delete(ws.clients, sock)
						ws.HandleSockClose(sock)
						ws.CheckReadySend()
						continue SOCKLBL
					}
				}
			}
			ws.Unlock()

		// прекращаем работу
		case <-ws.doneChan:
			ws.stopNetServer()
			fmt.Println("CLOSE WEB SOCKET SERVER")
			return
		}
	}
}

func (ws *WebSockServer) handleMain(w http.ResponseWriter, r *http.Request) {
	// апгрейд соединения
	client, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Ошибка апгрейда WS", err)
	} else {
		permitConn := false
		if len(ws.curSetts.PermitClientIps) > 0 {
			for _, it := range ws.curSetts.PermitClientIps {
				fmt.Printf("'" + it + "'-'" + client.RemoteAddr().String() + "'")
				if strings.HasPrefix(client.RemoteAddr().String(), it) {
					permitConn = true
					break
				}
			}
		} else {
			permitConn = true
		}

		if permitConn {
			ws.clients[client] = true
			ws.ClntConnChan <- client
			ws.StateChan <- ServerReadySend

			go func(curSock *websocket.Conn) {
				for {
					if _, data, err := curSock.ReadMessage(); err != nil {
						ws.ErrorChan <- fmt.Errorf("Ошибка чтения данных от клиента с адресом; <%s>. Ошибка: <%s>.", curSock.RemoteAddr().String(), err.Error())
						ws.HandleSockClose(curSock)
						ws.CheckReadySend()
						return
					} else {
						ws.ReceiveDataChan <- WsPackage{Data: data, Sock: curSock}
					}
				}
			}(client)
		} else {
			if err := client.Close(); err != nil {
				ws.ErrorChan <- fmt.Errorf("Ошибка закрытия клиентского подключения из-за недопустимого адреса клиента. Ошибка: <%s>.", err.Error())
			} else {
				ws.ErrorChan <- fmt.Errorf("Закрыто клиентское подключение, недопустимый адрес клиента.")
			}
			return
		}
	}
}

func (ws *WebSockServer) stopNetServer() {
	for sock := range ws.clients {
		ws.HandleSockClose(sock)
	}
	ws.CheckReadySend()

	if err := ws.server.Close(); err != nil {
		ws.ErrorChan <- fmt.Errorf("Ошибка закрытия WS сервера.", err.Error())
	}
	ws.server = nil
	ws.StateChan <- ServerError
}

// IsIPConnected предоставляет сведения о состоянии подключения клиента с указанным IP адресом
func (ws *WebSockServer) IsIPConnected(clientIP string) bool {
	for key := range ws.clients {
		if strings.HasPrefix(key.RemoteAddr().String(), clientIP) {
			return true
		}
	}
	return false
}

// SendClntList отправка списка подключенных клиентов
func (ws *WebSockServer) ClientList() []string {
	var retValue []string
	for key := range ws.clients {
		retValue = append(retValue, key.RemoteAddr().String())
	}
	return retValue
}

// HandleSockClose
func (ws *WebSockServer) HandleSockClose(sock *websocket.Conn) {
	ws.ClntDisconnChan <- sock
	sock.Close()
	delete(ws.clients, sock)
}

// CheckReadySend
func (ws *WebSockServer) CheckReadySend() {
	if len(ws.clients) > 0 {
		ws.StateChan <- ServerReadySend
	} else {
		ws.StateChan <- ServerNotReadySend
	}
}
