package log_std

import (
	"database/sql"
	"fmt"
	"log"
	"runtime"

	"fdps/utils/logger"
)

const (
	//timeFormat = "2006-01-02 15:04:05.000"

	// серьезность сообщения - отладка
	debugSev = "Отладка\t\t"
	// серьезность сообщения - информация
	infoSev = "Информация\t"
	// серьезность сообщения - предупреждение
	warningSev = "Предупреждение"
	// серьезность сообщения - ошибка
	errorSev = "Ошибка\t\t"
)

// LogStd экземпляр логгера, выводящего сообщения в консоль
var LogStd StdLogger = StdLogger{minSeverity: logger.SevDebug}

// StdLogger логгер, выводящий сообщения в консоль
type StdLogger struct {
	minSeverity logger.Severity // минимальная серъезность сообщений
}

// Printf реализация интерфейса logger
func (s StdLogger) Printf(format string, a ...interface{}) {
	if LogStd.minSeverity <= logger.SevInfo {
		printfCommon(infoSev, fmt.Sprintf(format, a...))
	}
}

// PrintfDebug реализация интерфейса logger
func (s StdLogger) PrintfDebug(format string, a ...interface{}) {
	if LogStd.minSeverity <= logger.SevDebug {
		printfCommon(debugSev, fmt.Sprintf(format, a...))
	}
}

// PrintfInfo реализация интерфейса logger
func (s StdLogger) PrintfInfo(format string, a ...interface{}) {
	if LogStd.minSeverity <= logger.SevInfo {
		printfCommon(infoSev, fmt.Sprintf(format, a...))
	}
}

// PrintfWarn реализация интерфейса logger
func (s StdLogger) PrintfWarn(format string, a ...interface{}) {
	if LogStd.minSeverity <= logger.SevWarning {
		printfCommon(warningSev, fmt.Sprintf(format, a...))
	}
}

// PrintfErr реализация интерфейса logger
func (s StdLogger) PrintfErr(format string, a ...interface{}) {
	if LogStd.minSeverity <= logger.SevError {
		printfCommon(errorSev, fmt.Sprintf(format, a...))
	}
}

// PrintfInfoDirType - PrintfInfo с типом и направлением сообщения
func (s StdLogger) PrintfInfoDirType(tp string, direction string, format string, a ...interface{}) {
	if LogStd.minSeverity <= logger.SevInfo {
		printfCommon(infoSev, fmt.Sprintf(format, a...))
	}
}

// SetDebugParam задать параметр и его значение для отображение в таблице
func (s StdLogger) SetDebugParam(paramName string, paramVal string, paramColor string) {
	//if LogStd.minSeverity <= logger.SevDebug {
	//	printfCommon(debugSev, fmt.Sprintf("Задано значение параметра отладки. Параметр: %s. Значение: %s.", paramName, paramVal))
	//}
}

// SetDbStats задать параметры состояния БД
func (s StdLogger) SetDbStats(dbStat sql.DBStats) {
	//if s.minSeverity <= logger.SevDebug {
	//	printfCommon(debugSev, fmt.Sprintf("Заданы параметры состояния БД: %#v.", dbStat))
	//}
}

// SetMinSeverity задать серъезность, начиная с которой будут вестись логи
func (s StdLogger) SetMinSeverity(sev logger.Severity) {
	LogStd.minSeverity = sev
}

// общая часть вывода лога в файл
func printfCommon(msgSev string, msgText string) {
	//curMsgText := time.Now().UTC().Format(timeFormat)
	//curMsgText += "\t"
	curMsgText := msgSev
	curMsgText += "\t"
	curMsgText += msgText
	curMsgText += "\n"
	_, file, line, _ := runtime.Caller(3)
	log.Println(file, line, curMsgText)
}
