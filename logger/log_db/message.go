package log_db

const (
	SeverityDebug   string = "Отладка"        // серьезность DEBUG.
	SeverityInfo    string = "Информация"     // серьезность INFO.
	SeverityWarning string = "Предупреждение" // серьезность WARNING.
	SeverityError   string = "Ошибка"         // серьезность ERROR.

	// направления сообщений
	DirectionIncoming  string = "Входящее"
	DirectionOutcoming string = "Исходящее"
	DirectionUnknown   string = "-"

	ChannelTypeNone string = "-"
)

// описание сообщенияя для журнала, получаемого по сети
type LogMessage struct {
	ControllerIP   string `json:"ControllerIP"` // IP адрес контроллера.
	Source         string `json:"Source"`       // название источника.
	ChannelId      int    `json:"DaemonID"`     // идентификатор канала.
	ChannelLocName string `json:"LocalName"`    // локальное имя канала.
	ChannelRemName string `json:"RemoteName"`   // удаленное имя канала.
	DataType       string `json:"DataType"`     // тип сообщений поверх FMTP.
	Severity       string `json:"Severity"`     // серьезность.
	FmtpType       string `json:"FmtpType"`     // тип FMTP пакета.
	Direction      string `json:"Direction"`    // направление сообщения.
	Text           string `json:"Text"`         // текст сообщения.
	DateTime       string `json:"DateTime"`     // дата и время сообщения.
}


