package log_db

import (
	"time"
)

const (
	// источники сообщений сообщений
	SourceAftnChannel    string = "FMTP канал"
	SourceAftnController string = "Контроллер"

	NoAftnChannelIdent        int    = -1  // идентификатор канала для "Контроллера"
	AftnCntrlIdentText string = "-" // текст идентификатора канала для "Контроллера"

	NoAftnChannelLocName string = "-"
	NoAftnChannelRemName string = "-"
)

// сообщение с использование ST (Severity-Text)
func LogAftnChannelST(severity string, text string) LogMessage {
	return LogMessage{
		Source:    SourceChannel,
		Severity:  severity,
		FmtpType:  NoneFmtpType,
		Direction: DirectionUnknown,
		Text:      text,
		DateTime:  time.Now().Format("2006-01-02 15:04:05.000")}
}

// сообщение с использование STDT (Severity-Type-Direction-Text)
func LogChannelSTDT(severity string, fmtpType string, direction string, text string) LogMessage {
	return LogMessage{
		Source:    SourceChannel,
		Severity:  severity,
		FmtpType:  fmtpType,
		Direction: direction,
		Text:      text,
		DateTime:  time.Now().Format("2006-01-02 15:04:05.000")}
}

// сообщение с использование IST (ID-Severity-Text)
func LogCntrlST(severity string, text string) LogMessage {
	return LogMessage{
		ChannelId: NoChannelIdent,
		Source:    SourceController,
		Severity:  severity,
		FmtpType:  NoneFmtpType,
		Direction: DirectionUnknown,
		Text:      text,
		DateTime:  time.Now().Format("2006-01-02 15:04:05.000")}
}


// сообщение с использование STDT (Severity-Type-Direction-Text)
func LogCntrlSTDT(severity string, fmtpType string, direction string, text string) LogMessage {
	return LogMessage{
		ChannelId: NoChannelIdent,
		Source:    SourceController,
		Severity:  severity,
		FmtpType:  fmtpType,
		Direction: direction,
		Text:      text,
		DateTime:  time.Now().Format("2006-01-02 15:04:05.000")}
}
