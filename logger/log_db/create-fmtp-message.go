package log_db

import (
	"time"
)

const (
	// источники сообщений сообщений
	SourceChannel    string = "FMTP канал"
	SourceController string = "Контроллер"

	NoChannelIdent        int    = -1  // идентификатор канала для "Контроллера"
	CntrlChannelIdentText string = "-" // текст идентификатора канала для "Контроллера"

	NoChannelLocName string = "-"
	NoChannelRemName string = "-"

	NoneFmtpType string = "-"
)

// конструктор для использования к FMTP канале
func CreateFmtpMessage(severity string, packetType string, direction string, text string) LogMessage {
	var  retValue LogMessage
	retValue.ControllerIP = ""
	retValue.Source = SourceChannel
	retValue.ChannelId = NoChannelIdent
	retValue.ChannelLocName = NoChannelLocName
	retValue.ChannelRemName = NoChannelRemName
	retValue.DataType = ChannelTypeNone
	retValue.Severity = severity
	retValue.FmtpType = packetType
	retValue.Direction = direction
	retValue.Text = text
	retValue.DateTime = time.Now().Format("2006-01-02 15:04:05.333")
	return retValue
}


// сообщение с использование ST (Severity-Text)
func LogFmtpChannelST(severity string, text string) LogMessage {
	return LogMessage{
		Source:    SourceChannel,
		Severity:  severity,
		FmtpType:  NoneFmtpType,
		Direction: DirectionUnknown,
		Text:      text,
		DateTime:  time.Now().Format("2006-01-02 15:04:05.000")}
}

// сообщение с использование STDT (Severity-Type-Direction-Text)
func LogFmtpChannelSTDT(severity string, fmtpType string, direction string, text string) LogMessage {
	return LogMessage{
		Source:    SourceChannel,
		Severity:  severity,
		FmtpType:  fmtpType,
		Direction: direction,
		Text:      text,
		DateTime:  time.Now().Format("2006-01-02 15:04:05.000")}
}

// сообщение с использование IST (ID-Severity-Text)
func LogFmtpCntrlST(severity string, text string) LogMessage {
	return LogMessage{
		ChannelId: NoChannelIdent,
		Source:    SourceController,
		Severity:  severity,
		FmtpType:  NoneFmtpType,
		Direction: DirectionUnknown,
		Text:      text,
		DateTime:  time.Now().Format("2006-01-02 15:04:05.000")}
}

// сообщение с использование STDT (Severity-Type-Direction-Text)
func LogFmtpCntrlSTDT(severity string, fmtpType string, direction string, text string) LogMessage {
	return LogMessage{
		ChannelId: NoChannelIdent,
		Source:    SourceController,
		Severity:  severity,
		FmtpType:  fmtpType,
		Direction: direction,
		Text:      text,
		DateTime:  time.Now().Format("2006-01-02 15:04:05.000")}
}
