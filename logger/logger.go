package logger

import (
	"database/sql"
	"log"
)

type Severity int

const (
	// SevDebug серьезность сообщения - отладка
	SevDebug Severity = iota
	// SevInfo серьезность сообщения - информация
	SevInfo
	// SevWarning серьезность сообщения - предупреждение
	SevWarning
	// SevError серьезность сообщения - ошибка
	SevError
)

const (
	// StateDefaultColor цвет по умолчанию
	StateDefaultColor = "#FFFFFF"
	// StateOkColor цвет рабочего состояния
	StateOkColor = "#DFF7DE"
	// StateOkColor цвет состояния предупреждения
	StateWarningColor = "#F4EDBA"
	// StateErrorColor цвет не рабочего состояния
	StateErrorColor = "#F2C4CA"
)

// Logger интерфейс логгера
type Logger interface {
	Printf(format string, a ...interface{})
	PrintfDebug(format string, a ...interface{})
	PrintfInfo(format string, a ...interface{})
	PrintfWarn(format string, a ...interface{})
	PrintfErr(format string, a ...interface{})

	PrintfInfoDirType(tp string, direction string, format string, a ...interface{})

	SetDebugParam(paramName string, paramVal string, paramColor string)
	SetDbStats(dbStat sql.DBStats)
	SetMinSeverity(sev Severity)
}

var logg []Logger

// AppendLogger добавить логгер
func AppendLogger(lgg Logger) {
	logg = append(logg, lgg)
}

// Printf вывод сообщения в лог (серьезность по умолчанию)
func Printf(format string, a ...interface{}) {
	if len(logg) > 0 {
		for _, logIt := range logg {
			logIt.Printf(format, a...)
		}
	} else {
		log.Printf(format, a...)
	}
}

// PrintfDebug вывод сообщения в лог (серьезность Debug)
func PrintfDebug(format string, a ...interface{}) {
	if len(logg) > 0 {
		for _, logIt := range logg {
			logIt.PrintfDebug(format, a...)
		}
	} else {
		log.Printf(format, a...)
	}
}

// PrintfInfo вывод сообщения в лог (серьезность Information)
func PrintfInfo(format string, a ...interface{}) {
	if len(logg) > 0 {
		for _, logIt := range logg {
			logIt.PrintfInfo(format, a...)
		}
	} else {
		log.Printf(format, a...)
	}
}

// PrintfWarn вывод сообщения в лог (серьезность Warning)
func PrintfWarn(format string, a ...interface{}) {
	if len(logg) > 0 {
		for _, logIt := range logg {
			logIt.PrintfWarn(format, a...)
		}
	} else {
		log.Printf(format, a...)
	}
}

// PrintfErr вывод сообщения в лог (серьезность Error)
func PrintfErr(format string, a ...interface{}) {
	if len(logg) > 0 {
		for _, logIt := range logg {
			logIt.PrintfErr(format, a...)
		}
	} else {
		log.Printf(format, a...)
	}
}

// PrintfInfoDirType - PrintfInfo с типом и направлением сообщения
func PrintfInfoDirType(tp string, direction string, format string, a ...interface{}) {
	if len(logg) > 0 {
		for _, logIt := range logg {
			logIt.PrintfInfoDirType(tp, direction, format, a...)
		}
	} else {
		log.Printf(format, a...)
	}
}

// SetDebugParam задание параметра для отладки
func SetDebugParam(paramName string, paramVal string, paramColor string) {
	if len(logg) > 0 {
		for _, logIt := range logg {
			logIt.SetDebugParam(paramName, paramVal, paramColor)
		}
	} else {
		log.Printf("Задано значение параметра отладки. Параметр: %s. Значение: %s.", paramName, paramVal)
	}
}

// SetDbStats задать параметры состояния БД
func SetDbStats(dbStat sql.DBStats) {
	if len(logg) > 0 {
		for _, logIt := range logg {
			logIt.SetDbStats(dbStat)
		}
	} else {
		log.Printf("Заданы параметры состояния БД: %м.", dbStat)
	}
}

// SetDbStats задать параметры состояния БД
func SetMinSeverity(sev Severity) {
	for _, logIt := range logg {
		logIt.SetMinSeverity(sev)
	}
}
