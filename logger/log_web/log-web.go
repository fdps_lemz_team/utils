package log_web

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"runtime"
	"strconv"
	"time"

	"github.com/gorilla/mux"

	"fdps/utils/logger"
)

// LogWebSettings настройки логгера
type LogWebSettings struct {
	StartHttp    bool   // необходимость запуска http сервера
	NetPort      int    // номер порта http сервера
	LogURLPath   string // path, по которому будет доступен журнал
	Title        string // заголовок страницы
	ShowSetts    bool   // показывать кнопку с настройками
	SettsURLPath string // path, по которому будут доступны настройки
}

// LogWeb логгер, отображающий последние 100 сообщений на web страничке
type LogWeb struct {
	setts       LogWebSettings
	httpServer  http.Server
	page        *LogWebPage
	startTime   time.Time
	totalTime   timeInWork
	minSeverity logger.Severity // минимальная серъезность сообщений
	//rtPage      *RuntimeWebPage
}

// WebLogger екземпляр лоррега
var WebLogger LogWeb

func (s LogWeb) Path() string {
	return "/" + s.setts.LogURLPath
}

func (s LogWeb) Caption() string {
	return s.page.title()
}

func (s LogWeb) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return s.handlerMain
}

// Printf реализация интерфейса logger
func (s LogWeb) Printf(format string, a ...interface{}) {
	if s.minSeverity <= logger.SevInfo {
		AppendLogWithSeverity(infoSev, fmt.Sprintf(format, a...))
	}
}

// PrintfDebug реализация интерфейса logger
func (s LogWeb) PrintfDebug(format string, a ...interface{}) {
	if WebLogger.minSeverity <= logger.SevDebug {
		AppendLogWithSeverity(debugSev, fmt.Sprintf(format, a...))
	}
}

// PrintfInfo реализация интерфейса logger
func (s LogWeb) PrintfInfo(format string, a ...interface{}) {
	if WebLogger.minSeverity <= logger.SevInfo {
		AppendLogWithSeverity(infoSev, fmt.Sprintf(format, a...))
	}
}

// PrintfWarn реализация интерфейса logger
func (s LogWeb) PrintfWarn(format string, a ...interface{}) {
	if WebLogger.minSeverity <= logger.SevWarning {
		AppendLogWithSeverity(warningSev, fmt.Sprintf(format, a...))
	}
}

// PrintfErr реализация интерфейса logger
func (s LogWeb) PrintfErr(format string, a ...interface{}) {
	if WebLogger.minSeverity <= logger.SevError {
		AppendLogWithSeverity(errorSev, fmt.Sprintf(format, a...))
	}
}

// PrintfInfoDirType - PrintfInfo с типом и направлением сообщения
func (s LogWeb) PrintfInfoDirType(tp string, direction string, format string, a ...interface{}) {
	if WebLogger.minSeverity <= logger.SevInfo {
		AppendLogWithSeverity(infoSev, fmt.Sprintf(format, a...))
	}
}

// SetDebugParam задать параметр и его значение для отображение в таблице
func (s LogWeb) SetDebugParam(paramName string, paramVal string, paramColor string) {
	s.page.setDebugParam(paramName, paramVal, paramColor)
	//s.rtPage.setDebugParam(paramName, paramVal, paramColor)
}

// SetDbStats задать параметры состояния БД
func (s LogWeb) SetDbStats(dbStat sql.DBStats) {
	//s.rtPage.setDbStat(dbStat)
}

// SetMinSeverity задать серъезность, начиная с которой будут вестись логи
func (s LogWeb) SetMinSeverity(sev logger.Severity) {
	WebLogger.minSeverity = sev
}

// Initialize инициализация web страницы
//func Initialize(handleURL string, netPort int, title string, startHttp bool) {

func Initialize(settings LogWebSettings) {
	if settings.StartHttp {
		r := mux.NewRouter()
		r.HandleFunc("/"+settings.LogURLPath, WebLogger.handlerMain)
		//r.HandleFunc("/"+"rt", WebLogger.handlerRuntime)

		WebLogger = LogWeb{
			httpServer: http.Server{
				Handler:      r,
				Addr:         fmt.Sprintf(":%d", settings.NetPort),
				ReadTimeout:  10 * time.Second,
				WriteTimeout: 10 * time.Second,
			},
			setts: settings,
			//startHttp:   startHttp,
			minSeverity: logger.SevDebug,
		}
	} else {
		WebLogger = LogWeb{
			setts:       settings,
			minSeverity: logger.SevDebug,
		}
	}

	//WebLogger.handleURL = "/" + handleURL
	WebLogger.startTime = time.Now().UTC()

	WebLogger.page = new(LogWebPage)
	WebLogger.page.setTitle(settings.Title)
	WebLogger.page.initialize(settings.ShowSetts, settings.SettsURLPath)

	//WebLogger.rtPage = new(RuntimeWebPage)
	//WebLogger.rtPage.setTitle(settings.Title)
	//WebLogger.rtPage.initialize()

	if WebLogger.setts.StartHttp {
		go func() {
			err := WebLogger.httpServer.ListenAndServe()
			if err != nil {
				fmt.Printf("Listen and serve: %v", err)
			}
		}()
	}
}

// AppendLog добавить сообщение журнала
func AppendLog(msgText string) {
	WebLogger.page.appendLog(logWithColor{
		MstDateTime: time.Now().UTC().Format(timeFormat),
		MsgColor:    defaultColor,
		MsgSeverity: infoSev,
		MsgText:     msgText,
	})
}

// AppendLogWithSeverity добавить сообщение журнала и серьезность
func AppendLogWithSeverity(msgSev string, msgText string) {
	var msgColor string

	switch msgSev {
	case debugSev:
		msgColor = debugColor
	case infoSev:
		msgColor = infoColor
	case warningSev:
		msgColor = warningColor
	case errorSev:
		msgColor = errorColor
	default:
		msgColor = defaultColor
	}

	WebLogger.page.appendLog(logWithColor{
		MstDateTime: time.Now().UTC().Format(timeFormat),
		MsgColor:    msgColor,
		MsgSeverity: msgSev,
		MsgText:     msgText,
	})
}

// SetVersion задать версию софта
func SetVersion(vers string) {
	WebLogger.page.setVersion(vers)
}

// SetDockerVersion задать версию Docker service
func SetDockerVersion(vers string) {
	WebLogger.page.setDockerVersion(vers)
}

func (s *LogWeb) handlerMain(w http.ResponseWriter, r *http.Request) {
	s.updateTime()
	s.page.setWorkTime(s.totalTime)
	//s.rtPage.setWorkTime(s.totalTime)

	var curMemStat runtime.MemStats
	runtime.ReadMemStats(&curMemStat)

	var curMemUsage memUsage
	curMemUsage.fromRuntimeMemStat(curMemStat)

	s.page.setMemStat(curMemUsage)

	s.page.setGrCount(runtime.NumGoroutine())

	s.page.updateLogs()

	if err := s.page.template().ExecuteTemplate(w, "T", s.page); err != nil {
		log.Println("template ExecuteTemplate ERROR")
	}
}

//func (s *LogWeb) handlerRuntime(w http.ResponseWriter, r *http.Request) {
//	s.updateTime()
//	s.rtPage.setWorkTime(s.totalTime)
//
//	var curMemStat runtime.MemStats
//	runtime.ReadMemStats(&curMemStat)
//
//	var curMemUsage memUsage
//	curMemUsage.fromRuntimeMemStat(curMemStat)
//
//	s.rtPage.setMemStat(curMemUsage)
//
//	s.rtPage.setGrCount(runtime.NumGoroutine())
//
//	s.rtPage.updateRuntime()
//
//	if err := s.rtPage.template().ExecuteTemplate(w, "RT", s.rtPage); err != nil {
//		log.Println("template ExecuteTemplate ERROR")
//	}
//}

func (s *LogWeb) updateTime() {
	s.page.Lock()
	defer s.page.Unlock()

	//s.rtPage.Lock()
	//defer s.rtPage.Unlock()

	secDuration := uint64(time.Since(s.startTime).Seconds())
	s.totalTime.Days = strconv.FormatUint(secDuration/(24*60*60), 10)
	s.totalTime.Hours = strconv.FormatUint((secDuration/(60*60))%24, 10)
	s.totalTime.Mins = strconv.FormatUint((secDuration/60)%60, 10)
	s.totalTime.Secs = strconv.FormatUint(secDuration%60, 10)
	s.totalTime.Now = time.Now().UTC().Format(timeFormat)
}
