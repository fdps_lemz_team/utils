package log_web

import (
	"container/list"
	"html/template"
	"log"
	"sort"
	"sync"
)

// LogWebPage страничка с отладочной информацией и логами
type LogWebPage struct {
	sync.RWMutex
	templ     *template.Template
	Title     string
	TotalTime timeInWork
	Lr        []*logWithColor
	logList   *list.List

	GoroutineCount int      // кол-во goroutines
	MemStat        memUsage // параметры использования памяти
	SoftVersion    string   // версия софта
	DockerVersion  string   // версия docker service

	// DebugInfo отладочная информация
	// выводит в таблице вместе с информцией runtime
	DebugInfo    []DebugValue
	DebugInfoMap map[string]DebugValue
	ShowSetts    bool
	SettsUrlPath string
}

// инициализация страницы
func (m *LogWebPage) initialize(showSetts bool, settsUrlPath string) {
	m.Lock()
	defer m.Unlock()

	m.logList = list.New()

	var err error
	if m.templ, err = template.New("MainPage").Parse(logWebPageTemplate); err != nil {
		log.Printf("Ошибка HTML шаблона: %v", err)
	}

	m.Lr = make([]*logWithColor, logSizeMax, logSizeMax)

	for i := 0; i < logSizeMax; i++ {
		m.logList.PushFront(&logWithColor{MsgColor: defaultColor})
	}
	m.DebugInfoMap = make(map[string]DebugValue)
	m.DebugInfo = make([]DebugValue, 0)
	m.ShowSetts = showSetts
	m.SettsUrlPath = settsUrlPath
}

// задать кол-во горутин
func (m *LogWebPage) setGrCount(curGrCount int) {
	m.GoroutineCount = curGrCount
}

// задать параметры использования памяти
func (m *LogWebPage) setMemStat(curMem memUsage) {
	m.MemStat = curMem
}

// задать заголовок страницы
func (m *LogWebPage) setTitle(title string) {
	m.Title = title
}

// задать версию софта
func (m *LogWebPage) setVersion(vers string) {
	m.SoftVersion = vers
}

// задать версию Docker service
func (m *LogWebPage) setDockerVersion(vers string) {
	m.DockerVersion = vers
}

// задать время работы
func (m *LogWebPage) setWorkTime(curTime timeInWork) {
	m.TotalTime = curTime
}

// предоставить шаблон страницы
func (m *LogWebPage) template() *template.Template {
	return m.templ
}

func (m *LogWebPage) title() string {
	return m.Title
}

// добавить сообщение журнала
func (m *LogWebPage) appendLog(message logWithColor) {
	m.Lock()
	defer m.Unlock()

	m.logList.PushFront(&message)

	for m.logList.Len() > logSizeMax {
		m.logList.Remove(m.logList.Back())
	}
}

// обновить сообщения журнала для отображения в таблице
func (m *LogWebPage) updateLogs() {
	for e, i := m.logList.Front(), 0; e != nil && i < logSizeMax; e, i = e.Next(), i+1 {
		lr, ok := e.Value.(*logWithColor)
		if ok {
			m.Lr[i] = lr
		}
	}
	// обновляем масиив с debug параметрами
	m.DebugInfo = m.DebugInfo[:0]
	for _, val := range m.DebugInfoMap {
		m.DebugInfo = append(m.DebugInfo, val)
	}

	sort.Slice(m.DebugInfo, func(i, j int) bool {
		return m.DebugInfo[i].Key <= m.DebugInfo[j].Key
	})

	curLen := len(m.DebugInfo)
	if len(m.DebugInfo) < maxDebugParams {
		for nn := 0; nn < maxDebugParams-curLen; nn++ {
			m.DebugInfo = append(m.DebugInfo, DebugValue{})
		}
	}
}

// задать параметр и его значение для отображение в таблице
func (m *LogWebPage) setDebugParam(paramKey string, paramVal string, paramColor string) {
	m.Lock()
	defer m.Unlock()
	if _, ok := m.DebugInfoMap[paramKey]; ok {
		m.DebugInfoMap[paramKey] = DebugValue{Key: paramKey, Value: paramVal, Color: paramColor}
	} else {
		if len(m.DebugInfoMap) < maxDebugParams {
			m.DebugInfoMap[paramKey] = DebugValue{Key: paramKey, Value: paramVal, Color: paramColor}
		}
	}
}

// шаблон страницы
var logWebPageTemplate = `{{define "T"}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{.Title}}</title>
<script>
//    window.onload = function() {
//        setTimeout(function () {
//            location.reload()
//        }, 1000);
//     };
</script>
</head>
<body>

<font size="3" face="verdana" color="black">
	<table width="100%" border="1" cellspacing="0" cellpadding="4">
		<colgroup>
			<col style = "width: 10%;">
			<col style = "width: 10%;">
			<col style = "width: 10%;">
			<col style = "width: 10%;">
			<col style = "width: 5%;">
			<col style = "width: 5%;">
			<col style = "width: 7%;">
			<col style = "width: 7%;">
		</colgroup>	
		<tr>
			{{with index .DebugInfo 0}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 0}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			{{with index .DebugInfo 1}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 1}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			<td>В работе:</td>
			<td>{{with .TotalTime}}{{.Days}} дн. {{.Hours}} ч. {{.Mins}} м. {{.Secs}} с.{{end}}</td>
			<td>Сейчас:</td>
			<td>{{with .TotalTime}}{{.Now}}(UTC){{end}}</td>
		</tr>
		<tr>
			{{with index .DebugInfo 2}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 2}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			{{with index .DebugInfo 3}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 3}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			<td>Кол-во goroutine:</td>
			<td>{{.GoroutineCount}}</td>
			<td>Кол-во вызовов GC:</td>
			<td>{{with .MemStat}} {{.NumGC}} {{end}}</td>
		</tr>
		<tr>
			{{with index .DebugInfo 4}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 4}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			{{with index .DebugInfo 5}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 5}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			<td>Память (Sys):</td>
			<td>{{with .MemStat}} {{.Sys}} MB {{end}}</td>
			<td>Кол-во объектов в памяти:</td>
			<td>{{with .MemStat}} {{.HeapObjects}} {{end}}</td>
		</tr>
		<tr>
			{{with index .DebugInfo 6}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 6}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			{{with index .DebugInfo 7}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 7}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			<td>Память (HeapInuse)</td>
			<td>{{with .MemStat}} {{.HeapInuse}} MB {{end}}</td>
			<td>Память (StackInuse):</td>
			<td>{{with .MemStat}} {{.StackInuse}} MB {{end}}</td>
		</tr>
		<tr>
			{{with index .DebugInfo 8}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 8}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			{{with index .DebugInfo 9}}<td bgcolor="{{.Color}}">{{.Key}}</td>{{end}}
			{{with index .DebugInfo 9}}<td bgcolor="{{.Color}}">{{.Value}}</td>{{end}}
			<td>Версия ПО:</td>
			<td>{{.SoftVersion}}</td>
			<td>Версия Docker:</td>
			<td>{{.DockerVersion}}</td>
		</tr>
	</table>

{{if .ShowSetts}}
	<form action="/{{.SettsUrlPath}}" method="POST">
    <input type="submit" value="Настройки">
	</form>
{{end}}

<p style="font-weight:bold">Сообщения журнала</p>

<table width="100%" border="1" cellspacing="0" cellpadding="4" >	
	<colgroup>
		<col span="1" style="width: 15%;">
		<col span="1" style="width: 5%;">
		<col span="1" style="width: 80%;">       	
    </colgroup>
	<tr>
		<th>Дата, время</th>
		<th>Серьезность</th>    	
		<th>Текст</th>
   	</tr>
	{{with .Lr}}
		{{range .}}
			<tr align="center" bgcolor="{{.MsgColor}}">	
				<td align="left">
					{{.MstDateTime}}
				</td>	
				<td align="left">
					{{.MsgSeverity}}
				</td>
				<td align="left">
					{{.MsgText}}
				</td>				
			</tr>
		{{end}}
	{{end}}
</table>
</body>
</html>
{{end}}
`

//{{with .Sl}}<td>{{index .Sl 1 .Value}}</td>{{end}}
