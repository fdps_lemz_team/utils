package log_web

import (
	"runtime"
	"strconv"
)

const (
	// bytesInMB кол-во байт в 1 bytesInMB
	bytesInMB float64 = 1 << 20

	logSizeMax = 100 // максимальное число отображаемых логов в таблице

	maxDebugParams = 10 // максимальное число отладочных параметров

	// LogDebugSev серьезность сообщения - отладка
	debugSev = "Отладка"
	// LogInfoSev серьезность сообщения - информация
	infoSev = "Информация"
	// LogWarningSev серьезность сообщения - предупреждение
	warningSev = "Предупреждение"
	// LogErrorSev серьезность сообщения - ошибка
	errorSev = "Ошибка"

	timeFormat = "2006-01-02 15:04:05.000"

	// DefaultColor цвет по умолчанию
	defaultColor = "#EAECEE"

	debugColor   = "#CFDFF9" // цвет отладочного сообщения
	infoColor    = "#DFF7DE" // цвет информационного сообщения
	warningColor = "#F4EDBA" // цвет предупреждающего сообщения
	errorColor   = "#F2C4CA" // цвет сообщения об ошибке
)

// структура для хранения значений времени работы с запуска и текущего времени
type timeInWork struct {
	Days  string
	Hours string
	Mins  string
	Secs  string
	Now   string
}

// структура для хранения параметров использования памяти
type memUsage struct {
	Sys          string // MB
	TotalAlloc   string // MB
	Alloc        string // MB
	HeapAlloc    string // MB
	HeapSys      string // MB
	HeapIdle     string // MB
	HeapInuse    string // MB
	HeapReleased string // MB
	StackInuse   string // MB
	StackSys     string // MB
	OtherSys     string // MB
	NumGC        uint32
	HeapObjects  uint64
}

// преобразование из runtime.MemStats в вид, отображаемый на страничке
func (mu *memUsage) fromRuntimeMemStat(memStat runtime.MemStats) {
	mu.Sys = strconv.FormatFloat(float64(memStat.Sys)/bytesInMB, 'g', 3, 64)
	mu.TotalAlloc = strconv.FormatFloat(float64(memStat.TotalAlloc)/bytesInMB, 'g', 3, 64)
	mu.Alloc = strconv.FormatFloat(float64(memStat.Alloc)/bytesInMB, 'g', 3, 64)
	mu.HeapAlloc = strconv.FormatFloat(float64(memStat.HeapAlloc)/bytesInMB, 'g', 3, 64)
	mu.HeapSys = strconv.FormatFloat(float64(memStat.HeapSys)/bytesInMB, 'g', 3, 64)
	mu.HeapIdle = strconv.FormatFloat(float64(memStat.HeapIdle)/bytesInMB, 'g', 3, 64)
	mu.HeapInuse = strconv.FormatFloat(float64(memStat.HeapInuse)/bytesInMB, 'g', 3, 64)
	mu.HeapReleased = strconv.FormatFloat(float64(memStat.HeapReleased)/bytesInMB, 'g', 3, 64)
	mu.StackInuse = strconv.FormatFloat(float64(memStat.StackInuse)/bytesInMB, 'g', 3, 64)
	mu.StackSys = strconv.FormatFloat(float64(memStat.StackSys)/bytesInMB, 'g', 3, 64)
	mu.OtherSys = strconv.FormatFloat(float64(memStat.OtherSys)/bytesInMB, 'g', 3, 64)
	mu.NumGC = memStat.NumGC
	mu.HeapObjects = memStat.HeapObjects
}

// DebugValue отладочная информация
// выводит в таблице вместе с информцией runtime
type DebugValue struct {
	Key   string // параметр
	Value string // значение параметра
	Color string // подсветка значения
}

// сообщение журнала с цветом
type logWithColor struct {
	MstDateTime string // дата, сремя возникновения сообщения
	MsgColor    string // цвет в таблице логов
	MsgSeverity string // серьезность сообщения
	MsgText     string // текст сообщения
}
