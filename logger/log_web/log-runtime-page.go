package log_web

import (
	"database/sql"
	"html/template"
	"log"
	"sort"
	"sync"
)

// RuntimeWebPage страничка отображения потребляемых ресурсов и состояния БД
type RuntimeWebPage struct {
	sync.RWMutex
	templ     *template.Template
	Title     string
	TotalTime timeInWork

	DbStats sql.DBStats
	WaitNewConn int // мсек

	GoroutineCount int      // кол-во goroutines
	MemStat        memUsage // параметры использования памяти

	DebugInfo    []DebugValue
	DebugInfoMap map[string]DebugValue
}

// инициализация страницы
func (m *RuntimeWebPage) initialize() {
	m.Lock()
	defer m.Unlock()

	var err error
	if m.templ, err = template.New("RuntimePage").Parse(runtimeWebPageTemplate); err != nil {
		log.Println("template Parse ERROR")
		return
	}

	m.DebugInfoMap = make(map[string]DebugValue)
	m.DebugInfo = make([]DebugValue, 0)
}

// задать кол-во горутин
func (m *RuntimeWebPage) setGrCount(curGrCount int) {
	m.GoroutineCount = curGrCount
}

// задать параметры использования памяти
func (m *RuntimeWebPage) setMemStat(curMem memUsage) {
	m.MemStat = curMem
}

// задать заголовок страницы
func (m *RuntimeWebPage) setTitle(title string) {
	m.Title = title
}

// задать параметры состояния БД
func (m *RuntimeWebPage) setDbStat(dbStat sql.DBStats) {
	m.DbStats = dbStat
}

// задать параметр и его значение для отображение в таблице
func (m *RuntimeWebPage) setDebugParam(paramKey string, paramVal string, paramColor string) {
	m.Lock()
	defer m.Unlock()
	if _, ok := m.DebugInfoMap[paramKey]; ok {
		m.DebugInfoMap[paramKey] = DebugValue{Key: paramKey, Value: paramVal, Color: paramColor}
	} else {
		if len(m.DebugInfoMap) < 8 {
			m.DebugInfoMap[paramKey] = DebugValue{Key: paramKey, Value: paramVal, Color: paramColor}
		}
	}
}

// задать время работы
func (m *RuntimeWebPage) setWorkTime(curTime timeInWork) {
	m.TotalTime = curTime
}

// предоставить шаблон страницы
func (m *RuntimeWebPage) template() *template.Template {
	return m.templ
}

func (m *RuntimeWebPage) title() string {
	return m.Title
}

// обновить сообщения журнала для отображения в таблице
func (m *RuntimeWebPage) updateRuntime() {
	// обновляем масиив с debug параметрами
	m.DebugInfo = m.DebugInfo[:0]
	for _, val := range m.DebugInfoMap {
		m.DebugInfo = append(m.DebugInfo, val)
	}

	sort.Slice(m.DebugInfo, func(i, j int) bool {
		return m.DebugInfo[i].Key <= m.DebugInfo[j].Key
	})

	curLen := len(m.DebugInfo)
	if len(m.DebugInfo) < maxDebugParams {
		for nn := 0; nn < maxDebugParams-curLen; nn++ {
			m.DebugInfo = append(m.DebugInfo, DebugValue{})
		}
	}

	m.WaitNewConn = int(m.DbStats.WaitDuration.Milliseconds())
}

// шаблон страницы
var runtimeWebPageTemplate = `{{define "RT"}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{.Title}}</title>
<script>
//    window.onload = function() {
//        setTimeout(function () {
//            location.reload()
//        }, 1000);
//     };
</script>
</head>
<body>

<font size="3" face="verdana" color="black">
	<table width="100%" border="1" cellspacing="0" cellpadding="4">
		<colgroup>
			<col style = "width: 20%;">
			<col style = "width: 20%;">
			<col style = "width: 20%;">
			<col style = "width: 20%;">			
		</colgroup>	
		<tr>
			<td>В работе:</td>
			<td>{{with .TotalTime}}{{.Days}} дн. {{.Hours}} ч. {{.Mins}} м. {{.Secs}} с.{{end}}</td>
			<td>Сейчас:</td>
			<td>{{with .TotalTime}}{{.Now}}(UTC){{end}}</td>
		</tr>
	</table>

	<p style="font-weight:bold">Потребляемые ресурсы</p>

	<table width="100%" border="1" cellspacing="0" cellpadding="4">
		<colgroup>
			<col style = "width: 20%;">
			<col style = "width: 20%;">
			<col style = "width: 20%;">
			<col style = "width: 20%;">			
		</colgroup>	
		<tr>
			<td>Кол-во goroutine:</td>
			<td>{{.GoroutineCount}}</td>
			<td>Кол-во вызовов GC:</td>
			<td>{{with .MemStat}} {{.NumGC}} {{end}}</td>
		</tr>
		<tr>
			<td>Память (Sys):</td>
			<td>{{with .MemStat}} {{.Sys}} MB {{end}}</td>
			<td>Кол-во объектов в памяти:</td>
			<td>{{with .MemStat}} {{.HeapObjects}} {{end}}</td>
		</tr>
		<tr>
			<td>Память (HeapInuse)</td>
			<td>{{with .MemStat}} {{.HeapInuse}} MB {{end}}</td>
			<td>Память (StackInuse):</td>
			<td>{{with .MemStat}} {{.StackInuse}} MB {{end}}</td>
		</tr>	
	</table>

	<p style="font-weight:bold">Состояния БД</p>

	<table width="100%" border="1" cellspacing="0" cellpadding="4">
		<colgroup>
			<col style = "width: 60%;">
			<col style = "width: 40%;">
			<col style = "width: 20%;">
			<col style = "width: 20%;">			
		</colgroup>	
		<tr>
			<td>Maximum number of open connections to the database:</td>
			<td>{{with .DbStats}} {{.MaxOpenConnections}} {{end}}</td>
		</tr>
		<tr>
			<td>The number of established connections both in use and idle:</td>
			<td>{{with .DbStats}} {{.OpenConnections}} {{end}}</td>
		</tr>
		<tr>
			<td>The number of connections currently in use:</td>
			<td>{{with .DbStats}} {{.InUse}} {{end}}</td>
		</tr>
		<tr>
			<td>The number of idle connections:</td>
			<td>{{with .DbStats}} {{.Idle}} {{end}}</td>
		</tr>
		<tr>
			<td>The total number of connections waited for:</td>
			<td>{{with .DbStats}} {{.WaitCount}} {{end}}</td>
		</tr>
		<tr>
			<td>The total time blocked waiting for a new connection:</td>
			<td>{{.WaitNewConn}} мсек.</td>
		</tr>
		<tr>
			<td>The total number of connections closed due to SetMaxIdleConns:</td>
			<td>{{with .DbStats}} {{.MaxIdleClosed}} {{end}}</td>
		</tr>
		<tr>
			<td>The total number of connections closed due to SetConnMaxLifetime:</td>
			<td>{{with .DbStats}} {{.MaxLifetimeClosed}} {{end}}</td>
		</tr>	
		
	</table>

</body>
</html>
{{end}}
`

//{{with .Sl}}<td>{{index .Sl 1 .Value}}</td>{{end}}
