package log_ljack

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"gopkg.in/natefinch/lumberjack.v2"

	"fdps/utils/logger"
)

const (
	timeFormat = "2006-01-02 15:04:05.000"

	// серьезность сообщения - отладка
	debugSev = "Отладка\t\t"
	// серьезность сообщения - информация
	infoSev = "Информация\t"
	// серьезность сообщения - предупреждение
	warningSev = "Предупреждение"
	// серьезность сообщения - ошибка
	errorSev = "Ошибка\t\t"
)

// LogLjack экземпляр логгера
var LogLjack LjackLogger

var fileLogger lumberjack.Logger

// LjackSettings настройки контроллера записи логов в файл
type LjackSettings struct {
	FileSizeMB    int             `json:"FileSizeMB"`    // размер файла в MБ
	MaxFilesCount int             `json:"MaxFilesCount"` // максимальное кол-во файлов
	LogStoreDays  int             `json:"StoreDays"`     // время хранения логов (дней)
	FilesPath     string          `json:"FilesPath"`     // путь к папке с файлами логов (относительно бинарника программы) в формате "/dir/subdir"
	FilesName     string          `json:"FilesName"`     // имя файла с логами в формате "/name.ext" (например "/fdps-aftn.log")
	minSeverity   logger.Severity // минимальная серъезность сообщений
}

// GenDefault настройки логгера ljack по умолчанию
func (fls *LjackSettings) GenDefault() {
	fls.FileSizeMB = 1
	fls.MaxFilesCount = 100
	fls.LogStoreDays = 30
	fls.FilesPath = "/logs/"
	fls.FilesName = "ljack.log"
	fls.minSeverity = logger.SevDebug
}

// LjackLogger логгер на основе lumberjack.v2
type LjackLogger struct {
	setts LjackSettings
}

// Printf реализация интерфейса logger
func (s LjackLogger) Printf(format string, a ...interface{}) {
	if LogLjack.setts.minSeverity <= logger.SevInfo {
		printfCommon(infoSev, fmt.Sprintf(format, a...))
	}
}

// PrintfDebug реализация интерфейса logger
func (s LjackLogger) PrintfDebug(format string, a ...interface{}) {
	if LogLjack.setts.minSeverity <= logger.SevDebug {
		printfCommon(debugSev, fmt.Sprintf(format, a...))
	}
}

// PrintfInfo реализация интерфейса logger
func (s LjackLogger) PrintfInfo(format string, a ...interface{}) {
	if LogLjack.setts.minSeverity <= logger.SevInfo {
		printfCommon(infoSev, fmt.Sprintf(format, a...))
	}
}

// PrintfWarn реализация интерфейса logger
func (s LjackLogger) PrintfWarn(format string, a ...interface{}) {
	if LogLjack.setts.minSeverity <= logger.SevWarning {
		printfCommon(warningSev, fmt.Sprintf(format, a...))
	}
}

// PrintfErr реализация интерфейса logger
func (s LjackLogger) PrintfErr(format string, a ...interface{}) {
	if LogLjack.setts.minSeverity <= logger.SevError {
		printfCommon(errorSev, fmt.Sprintf(format, a...))
	}
}

// PrintfInfoDirType - PrintfInfo с типом и направлением сообщения
func (s LjackLogger) PrintfInfoDirType(tp string, direction string, format string, a ...interface{}) {
	if LogLjack.setts.minSeverity <= logger.SevInfo {
		printfCommon(infoSev, fmt.Sprintf(format, a...))
	}
}

// SetDebugParam задать параметр и его значение для отображение в таблице
func (s LjackLogger) SetDebugParam(paramName string, paramVal string, paramColor string) {
	//if LogLjack.setts.minSeverity <= logger.SevDebug {
	//	printfCommon(debugSev, fmt.Sprintf("Задано значение параметра отладки. Параметр: %s. Значение: %s.", paramName, paramVal))
	//}
}

// SetDbStats задать параметры состояния БД
func (s LjackLogger) SetDbStats(dbStat sql.DBStats) {
	//if s.minSeverity <= logger.SevDebug {
	//	printfCommon(debugSev, fmt.Sprintf("Заданы параметры состояния БД: %#v.", dbStat))
	//}
}

// SetMinSeverity задать серъезность, начиная с которой будут вестись логи
func (s LjackLogger) SetMinSeverity(sev logger.Severity) {
	LogLjack.setts.minSeverity = sev
}

// Initialize инициализация логгера настройками
func Initialize(setts LjackSettings) error {
	LogLjack.setts = setts

	// директория, в которую будут складываться файлы с логами
	dir, dirErr := filepath.Abs(filepath.Dir(os.Args[0]))
	if dirErr != nil {
		return dirErr
	}
	dir += setts.FilesPath

	// создание директории, в которую будут складываться файлы с логами
	if mkErr := os.MkdirAll(dir, os.ModePerm); mkErr != nil {
		return mkErr
	}

	// инициализация логера
	fileLogger = lumberjack.Logger{
		Filename:   dir + LogLjack.setts.FilesName,
		MaxSize:    LogLjack.setts.FileSizeMB,    // megabytes
		MaxBackups: LogLjack.setts.MaxFilesCount, // count files
		MaxAge:     LogLjack.setts.LogStoreDays,  // days
		Compress:   true,                         // disabled by default
	}

	// выставляем микросекунды в записи времени логов
	//log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	// перенаправление логов
	//log.SetOutput(&fileLogger)

	//log.SetOutput(os.Stdout)
	return nil
}

// общая часть вывода лога в файл
func printfCommon(msgSev string, msgText string) {
	curMsgText := time.Now().UTC().Format(timeFormat)
	curMsgText += "\t"
	curMsgText += msgSev
	curMsgText += "\t"
	curMsgText += msgText
	curMsgText += "\n"

	fileLogger.Write([]byte(curMsgText))
}
