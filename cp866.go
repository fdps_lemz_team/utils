package utils

import (
	"golang.org/x/text/encoding/charmap"
	"log"
)

func Cp866ToUtf8(ba []byte) []byte {
	dec := charmap.CodePage866.NewDecoder()
	if out, err := dec.Bytes(ba); err != nil {
		log.Println(err)
		return nil
	} else {
		return out
	}
}

func Utf8ToCp866(ba []byte) []byte {
	enc := charmap.CodePage866.NewEncoder()
	if out, err := enc.String(string(ba)); err != nil {
		log.Println(err)
		return nil
	} else {
		return []byte(out)
	}
}
