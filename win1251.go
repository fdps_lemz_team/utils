package utils

import (
	"golang.org/x/text/encoding/charmap"
	"log"
)

func Win1251toUtf8(ba []byte) []byte {
	dec := charmap.Windows1251.NewDecoder()
	if out, err := dec.Bytes(ba); err != nil {
		log.Println(err)
		return nil
	} else {
		return out
	}
}

func Utf8toWin1251(ba []byte) []byte {
	enc := charmap.Windows1251.NewEncoder()
	if out, err := enc.String(string(ba)); err != nil {
		log.Println(err)
		return nil
	} else {
		return []byte(out)
	}
}
