package userhub_web

import (
	"time"

	"github.com/gorilla/websocket"
)

type Client struct {
	Sock         *websocket.Conn
	Key          string
	AppName      string
	IpAddr       []string
	Version      string
	LastActivity string
}

func FromWebSock(wsSock *websocket.Conn) Client {
	retValue := Client{
		Sock:   wsSock,
		IpAddr: []string{wsSock.RemoteAddr().String()},
	}
	retValue.MarkTime()
	return retValue
}

func (c *Client) MarkTime() {
	c.LastActivity = time.Now().Format("2006-01-02 15:04:05")
}
