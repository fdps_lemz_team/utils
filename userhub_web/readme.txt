1. В пакете main добавляем

    userhub_web.Initialize("clnt", "CLNT")
    utils.AppendHandler(userhub_web.ClntHdl)

    utils.AppendHandler надо выполнить до инициализации web сервера приложения

2. В контроллере webSocketServer при чтении из каналов добавляем 
    вызов функций ClientConn, ClientDisconn
    
    case curClnt := <-s.wsServer.ClntConnChan:
        userhub_web.ClientConn(userhub_web.FromWebSock(curClnt))        
        
    case curClnt := <-s.wsServer.ClntDisconnChan:
        userhub_web.ClientDisconn(userhub_web.FromWebSock(curClnt))
        
3. При приеме других сообщений вызываем ClientUpdate
    // в протоколе обмена для сообщений (HeartbeatMsg например) можно добавить функцию преобразовния в userhub_web.Client
    
    userhub_web.ClientUpdate(curHbtMsg.ToUserhubClient(curWsPkg.Sock)) // сокет клиента присылается в пакете от WsServer