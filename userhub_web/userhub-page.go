package userhub_web

import (
	"html/template"
	"log"
	"sync"

	"github.com/gorilla/websocket"
)

type ClntPage struct {
	sync.RWMutex
	Templ     *template.Template
	handleURL string
	Title     string

	clientsMap map[*websocket.Conn]Client
	Clients    []Client
}

func (cp *ClntPage) initialize() {
	cp.Lock()
	defer cp.Unlock()

	var err error
	if cp.Templ, err = template.New("ClientsPage").Parse(ClientsPageTemplate); err != nil {
		log.Println("template Parse ERROR")
		return
	}

	cp.Title = "Clients"

	cp.clientsMap = make(map[*websocket.Conn]Client)
}

var ClientsPageTemplate = `{{define "ClientsPage"}}
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>{{.Title}}</title>		
	</head>
	<body>
		<font size="3" face="verdana" color="black">

			<table width="100%" border="1" cellspacing="0" cellpadding="4" >
				<caption style="font-weight:bold">Клиенты {{.Title}}</caption>
				<tr>
					<th>Ключ</th>
					<th>Приложение</th>
					<th>IP адреса</th>				
					<th>Версия</th>							
					<th>Время последней активности</th>
				</tr>
				{{with .Clients}}
					{{range .}}
						<tr align="center">	
							<td align="left"> {{.Key}} </td>
							<td align="left"> {{.AppName}} </td>
							<td align="left"> {{range $tag := .IpAddr}}  {{$tag}},  {{end}} </td>
							<td align="left"> {{.Version}} </td>		
							<td align="left"> {{.LastActivity}} </td>
						</tr>
					{{end}}
				{{end}}
			</table>
		</font>
	</body>
</html>
{{end}}
`
