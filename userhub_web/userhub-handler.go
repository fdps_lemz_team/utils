package userhub_web

import (
	"fmt"
	"net/http"
	"sort"
)

type ClntHandler struct {
	handleURL string
	page      *ClntPage
}

var ClntHdl ClntHandler

func (cw ClntHandler) Path() string {
	return cw.handleURL
}

func (cw ClntHandler) Caption() string {
	return cw.page.Title
}

func (cw ClntHandler) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return cw.handlerMain
}

func ClientConn(c Client) {
	ClntHdl.page.clientsMap[c.Sock] = c
}

func ClientUpdate(c Client) {
	ClntHdl.page.clientsMap[c.Sock] = c
}

func ClientDisconn(c Client) {
	delete(ClntHdl.page.clientsMap, c.Sock)
}

func Initialize(handleURL string, title string) {
	ClntHdl.handleURL = "/" + handleURL
	ClntHdl.page = new(ClntPage)
	ClntHdl.page.Title = title
	ClntHdl.page.initialize()
}

func (cw *ClntHandler) handlerMain(w http.ResponseWriter, r *http.Request) {
	ClntHdl.page.Clients = ClntHdl.page.Clients[:0]
	for _, val := range ClntHdl.page.clientsMap {
		ClntHdl.page.Clients = append(ClntHdl.page.Clients, val)
	}
	sort.Slice(ClntHdl.page.Clients, func(i, j int) bool {
		return ClntHdl.page.Clients[i].Key <= ClntHdl.page.Clients[j].Key
	})

	if err := cw.page.Templ.ExecuteTemplate(w, "ClientsPage", cw.page); err != nil {
		fmt.Println("template ExecuteTemplate ERROR", err)
	}
}
