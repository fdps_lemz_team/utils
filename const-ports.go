package utils

// список сетевых портов сервисов FDPS
const (
	// FDPS-MONS-AC
	MonsAcWebDefaultPort = 8085

	////// FDPS-AFTN

	// AFTN контроллер
	AftnChiefWebDefaultPort = 8086
	AftnChiefChannelsPath   = "chief"
	AftnChiefLogPath        = "log"

	// начальный порт web старниц AFTN каналов (порт web страницы канала 8100 + ID)
	AftnChannelBeginPort = 8100
	AftnChannelsPath     = "log"

	// порт WebSocket сервера связи контроллера и каналов AFTN
	AftnChiefChannelWsPort = 20000

	// ЦКС имитатор
	AftnCcmImitLogPath  = "log"
	AftnCcmImitTlgPath  = "tlg"
	AftnCcmImitSendPath = "send"

	AftnFdpsWebDefaultPort = 8089
	AftnFdpsPath           = "log"
	AftnFdpsChannelPath    = "fdps"

	// FDPS-RVM
	RvmWebDefaultPort = 8087

	// TREN KSV-SYNC
	KsvSyncDefaultPort      = 42042
	KsvSyncDefaultWebPort   = 8042
	KsvSyncEditSettingsPath = "settings"
	KsvSyncSaveSettingsPath = "save"
	KsvSyncLogPath          = "log"

	// CCAMS-SRV
	CcamsDefaultWsPort  = 9090
	CcamsDefaultWebPort = 8090
	CcamsFdpsPath       = ""
	CcamsConfigPath     = "config"
	CcamsSaveConfigPath = "save"
	CcamsLogPath        = "log"
	CcamsSquawkPath     = "ssr"
	CcamsClientsPath    = "clnt"

	// FDPS-PARKING
	ParkingWebDefaultPort  = 8070
	ParkingWebLogPath      = "log"
	ParkingWebConfigPath   = "config"
	ParkingWebClientsPath  = "clients"
	ParkingWebParkingsPath = "parkings"
	ParkingWebImagePath    = "image"

	ParkingClientsPath = "parking"
	ParkingClientsPort = 8071

	// FDPS-STATISTICS
	StatisticsWebDefaultPort = 8072
	StatisticsWebLogPath     = "log"
	StatisticsWebConfigPath  = "config"
	StatisticsWebClientsPath = "clients"

	StatisticsClientsPath = "statistics"
	StatisticsClientsPort = 8073

	StatisticsCachePath = "cached_files"

	// FDPS-FMTP
	FmtpChiefWebLogPath    = "log"
	FmtpChiefWebLogPort    = 8060
	FmtpChiefWebConfigPath = "config"

	FmtpLoggerWsPath = "logger" // URL для работы с логгером

	FmtpChannelWsUrlPath = "channels" // URL для работы WS fmtp каналов

	FmtpAodbWsUrlPath = "" // URL для работы с провадером AODB

	FmtpChannelWebPath      = "channel" // URL для web странички канала
	FmtpChannelStartWebPort = 13100     // начальный порт для web страницы каналов

	FmtpChiefWebPath = "chief" // URL для web странички контроллера

)
