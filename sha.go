package utils

import (
	"crypto/sha256"
	"encoding/hex"
	"strings"
)

func GenSha(val string) string {
	h := sha256.New()
	h.Write([]byte(packStringForSha(val)))
	return hex.EncodeToString(h.Sum(nil))
}

func packStringForSha(val string) string {
	repStr := []string{"\r", "\n", "\t", "  "}
	for i := 0; i < len(repStr); i++ {
		for strings.Count(val, repStr[i]) > 0 {
			val = strings.Replace(val, repStr[i], " ", -1)
		}
	}
	return val
}
